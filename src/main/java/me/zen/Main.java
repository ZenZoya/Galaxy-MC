package me.zen;

import me.zen.block.TestBlockHandler;
import me.zen.block.placement.DripstonePlacementRule;
import me.zen.commands.*;
import me.zen.features.Features;
import me.zen.instance.LobbyInstance;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextColor;
import net.minestom.server.MinecraftServer;
import net.minestom.server.adventure.audience.Audiences;
import net.minestom.server.command.CommandManager;
import net.minestom.server.coordinate.Pos;
import net.minestom.server.entity.Player;
import net.minestom.server.event.entity.EntityAttackEvent;
import net.minestom.server.event.player.AsyncPlayerConfigurationEvent;
import net.minestom.server.event.player.PlayerSpawnEvent;
import net.minestom.server.event.GlobalEventHandler;
import net.minestom.server.event.server.ServerListPingEvent;
import net.minestom.server.event.server.ServerTickMonitorEvent;
import net.minestom.server.extras.lan.OpenToLAN;
import net.minestom.server.extras.lan.OpenToLANConfig;
import net.minestom.server.instance.block.BlockManager;
import net.minestom.server.monitoring.TickMonitor;
import net.minestom.server.ping.ResponseData;
import net.minestom.server.timer.TaskSchedule;
import net.minestom.server.utils.MathUtils;
import net.minestom.server.utils.identity.NamedAndIdentified;
import net.minestom.server.utils.time.TimeUnit;

import java.time.Duration;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicReference;

import static me.zen.config.ConfigHandler.CONFIG;
import static net.minestom.server.MinecraftServer.LOGGER;
import static net.minestom.server.MinecraftServer.getGlobalEventHandler;

public class Main {

    public static void main(String[] args) {
        // Server Init
        MinecraftServer.setCompressionThreshold(128);
        MinecraftServer minecraftServer = MinecraftServer.init();

        MinecraftServer.getBenchmarkManager().enable(Duration.of(10, TimeUnit.SECOND));
        MinecraftServer.setBrandName("Vortres");
        if (CONFIG.prometheus().enabled()) Metrics.init();

        // Events
        BlockManager blockManager = MinecraftServer.getBlockManager();
        blockManager.registerBlockPlacementRule(new DripstonePlacementRule());
        blockManager.registerHandler(TestBlockHandler.INSTANCE.getNamespaceId(), () -> TestBlockHandler.INSTANCE);

        // Command Manager
        CommandManager commandManager = MinecraftServer.getCommandManager();
        commandManager.register(new HealthCommand());
        commandManager.register(new ShutdownCommand());
        commandManager.register(new TeleportCommand());
        commandManager.register(new PlayersCommand());
        commandManager.register(new FindCommand());
        commandManager.register(new GiveCommand());
        commandManager.register(new SaveCommand());
        commandManager.register(new GamemodeCommand());
        commandManager.register(new SpawnCommand());
        commandManager.register(new SetBlockCommand());
        commandManager.register(new TestMessageHelper());
        commandManager.register(new SummonCommand());
        commandManager.register(new WorldCommand());

        commandManager.setUnknownCommandCallback((sender, command) -> sender.sendMessage(Component.text("> Unknown command", TextColor.color(255, 50, 50))));

        // Initialization
        Initialization.init();

        // Start Server
        minecraftServer.start("0.0.0.0", 25565);
        OpenToLAN.open(new OpenToLANConfig().eventCallDelay(Duration.of(1, TimeUnit.DAY)));
        Runtime.getRuntime().addShutdownHook(new Thread(MinecraftServer::stopCleanly));
    }
}